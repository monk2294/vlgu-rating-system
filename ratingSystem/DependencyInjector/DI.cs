﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityRatingSystem.Models;
using System.Threading;

namespace UniversityRatingSystem.DependencyInjector
{
    public class DI
    {
        // Singleton pattern
        private static DI singleton;
        public static DI Singleton
        {
            get
            {
                if (singleton == null)
                    singleton = new DI();
                return singleton;
            }
        }

        // Main routine
        private UserManager<ApplicationUser> userManager;
        private ApplicationDbContext appDbContext;
        public UserManager<ApplicationUser> UserManager
        {
            get
            {
                if(userManager == null)
                    userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(ApplicationDbContext));
                return userManager;
            }
        }

        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                if (appDbContext == null)
                    appDbContext = new ApplicationDbContext();
                return appDbContext;
            }
        }
    }
}