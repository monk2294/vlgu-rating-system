﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UniversityRatingSystem.Models
{
    public class Person
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public Int32 PersonId { get; set; }

        public int GroupId { get; set; }

        public string AppUserId { get; set; }


        public virtual ApplicationUser AspUser { get; set; }

        public virtual Group Group { get; set; }

        public virtual List<Mark> Marks { get; set; }

        public virtual List<Document> Documents { get; set; }
    }

    public class Group
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public Int32 GroupId { get; set; }

        public String Name { get; set; }

        public virtual List<Person> Students { get; set; }

        public virtual List<Document> Documents { get; set; }

    }

    public class Document
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public Int32 DocumentId { get; set; }

        public String Title { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }

        public int ApplicationUserId { get; set; }

        public string Columns { get; set; }

        public int GroupId { get; set; }

        public virtual Group Group { get; set; }

        public virtual ApplicationUser Creator { get; set; }
    }

    public class Mark
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public Int32 MarkId { get; set; }

        public Int32 ColIndex { get; set; }

        public int Ball { get; set; }

        public int PersonId { get; set; }

        public int DocumentId { get; set; }

        public virtual Person Student { get; set; }

        public virtual Document Document { get; set; }
    }
}