﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UniversityRatingSystem.Models
{
    public class AdminRegisterViewModel : RegisterViewModel
    {
        [Required]
        [Display(Name = "Функциональная роль", Description = "В приложении есть только четыре роли: administrator, teacher, student, starosta")]
        public string Role { get; set; }
    }

    public class AdminEditUserViewModel
    {
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Отчество")]
        public string Patronymic { get; set; }

        [Required]
        [Display(Name = "Шифр учебной группы", Description = "Пример: ИСТ-112")]
        public string Group { get; set; }

        [Required]
        [Display(Name = "Функциональная роль", Description = "В приложении есть только четыре роли: administrator, teacher, student, starosta")]
        public string Role { get; set; }
    }
}