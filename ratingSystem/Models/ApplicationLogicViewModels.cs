﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UniversityRatingSystem.Models
{
    public class DocumentStep1ViewModel
    {
        [Required]
        [Display(Name="Название документа")]
        public String Title { get; set; }

        [Required]
        [Display(Name="Шифр группы")]
        public String Group { get; set; }

        [Required]
        [Display(Name = "Количество колонок")]
        public int ColumnCount { get; set; }
    }

    public class ColumnDataInViewModel
    {
        [Required]
        [Display(Name="Название колонки")]
        public string Name;

        [Required]
        [Display(Name = "Максимальный балл")]
        public int MaxMark;

        [Required]
        [Display(Name = "Рейтинг")]
        public bool Rating;

        [Required]
        [Display(Name = "Итог")]
        public bool Summary;
    }

    public class DocumentStep2ViewModel : DocumentStep1ViewModel
    {
        public List<ColumnDataInViewModel> Columns;
    }

    public class EditableDocument
    {
        public Document Document;
        public List<ColumnDataInViewModel> Columns;
    }
}