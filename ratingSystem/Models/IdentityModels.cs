﻿using Microsoft.AspNet.Identity.EntityFramework;
﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace UniversityRatingSystem.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public string Group { get; set; }
        public bool Confirmed { get; set; }
        public string Email { get; set; }
        public virtual Person Person { get; set; }
        public virtual List<Document> Documents { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Mark>().HasRequired( f => f.Student ).WithRequiredDependent().WillCascadeOnDelete(false);
            modelBuilder.Entity<Document>().HasOptional(f => f.Creator).WithOptionalDependent().WillCascadeOnDelete(false);
            modelBuilder.Entity<ApplicationUser>().HasOptional(f => f.Person).WithOptionalDependent().WillCascadeOnDelete(false);
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Mark> Marks { get; set; }
    }
}